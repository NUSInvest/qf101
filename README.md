# QF101 on 20 October 2018

This is the code repository of QF101 held by NUSInvest QF Department in
20 October 2018. The topics are as follows:

- Stage 0 - Introduction to Algo Trading (Syakyr)
- Stage 1 - Introduction to VBA for Banking (Jiangyue & Wuhao)
- Stage 2 - Introduction to Python and How to Backtest a Simple Strategy
  in 45 Minutes (Syakyr)
- Stage 3 - Industrial Sharing by the Director of AY17/18 (Zexin)
- Stage 4 - How to Set Up an Algo Trading Desk (QuantInsti)
